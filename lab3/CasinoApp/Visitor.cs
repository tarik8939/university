using System;

namespace CasinoApp
{
abstract class Visitor
    {
        public abstract string Play();

    }
    class StandartVisitor : Visitor
    {
        public string type = "Standart visitor";
        public override string Play()
        {
            return $"{type}:wow i can playing a game";
        }
    }
    abstract class Decorator : Visitor
    {
        protected Visitor _visitor;
        public Decorator(Visitor visitor)
        {
            this._visitor = visitor;
        }
        public void VisitorDecorator(Visitor visitor)
        {
            this._visitor = visitor;
        }
        public override string Play()
        {
            if (this._visitor != null)
            {
                return this._visitor.Play();
            }
            else
            {
                return string.Empty;
            }
        }
    }
    class VIPVisitor : Decorator
    {
        public VIPVisitor(Visitor vst) : base(vst)
        {
        }

        public string type = "VIP visitor";
        private string GoToBar()
        {
            return "I'm gonna go to bar";
        }
        private string GoToRestaurant()
        {
            return "I'm gonna go to restaurant";
        }
        private string GoBack()
        {
            return "I don't want to go in bar and restaurant and bar";
        }
        public string Where()
        {
            Random r = new Random();
            int ind = r.Next(-1, 3);
            //Console.WriteLine(ind);
            if (ind == 0)
            {
                return GoBack();
            }
            else if (ind == 1)
            {
                return GoToBar();
            }
            else return GoToRestaurant();
        }
        //public override string Play()
        //{
        //    //string rez = $"wow i can playing a game \n {Where()}";
        //    return $"{type}:wow i can playing a game, hm.... {Where()}";
        //}
        public override string Play()
        {
            return $"{type}:{_visitor.Play()}, hmmm.....{Where()}";
        }
    }
}