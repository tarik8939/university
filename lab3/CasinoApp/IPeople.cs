namespace CasinoApp
{
    interface IPeople
    {
        string name { get; } 
        string surname { get; }
        int Age { get; }
    }
}