﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CasinoApp
{
    public partial class Form2 : Form
    {
        GameForm f1;
        GameForm f2;

        GameFormThreading ft1;
        GameFormThreading ft2;
        public Form2()
        {
            InitializeComponent();
            f1 = GameForm.getInstance();
            f2 = GameForm.getInstance();


        }

        private void Form2_Load(object sender, EventArgs e)
        {
            if (f1 == f2)
            {
                Console.WriteLine("Singleton works.");
                
            }
            else
            {
                Console.WriteLine("Singleton failed.");
            }



            Thread process1 = new Thread(() =>
            {
                TestSingleton("FOO");
            });
            Thread process2 = new Thread(() =>
            {
                TestSingleton("BAR");
            });

            process1.Start();
            process2.Start();

            process1.Join();
            process2.Join();
            
            facade facade = new facade();
            facade.SetAll();
        }

        public static void TestSingleton(string value)
        {
            GameFormThreading singleton = GameFormThreading.GetInstance(value);
            Console.WriteLine(singleton.Value);
        }


        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            string item = Convert.ToString(comboBox1.SelectedItem);

            if (item == "item1")
            {
                //Console.WriteLine("asdafaf");
                richTextBox1.AppendText(f1.Owner.BuyItem1(new InteriorItemFactory()));
                
            }
            else if (item == "item2")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem2(new InteriorItemFactory()));
            }
            else if (item == "item3")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem3(new InteriorItemFactory()));
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string item2 = Convert.ToString(comboBox2.SelectedItem);
            if (item2 == "item1")
            {
                richTextBox1.AppendText( f1.Owner.BuyItem1(new PlayingTableFactory()));
            }
            else if (item2 == "item2")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem2(new PlayingTableFactory()));
            }
            else if (item2 == "item3")
            {
               richTextBox1.AppendText( f1.Owner.BuyItem3(new PlayingTableFactory()));

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            richTextBox1.AppendText($"\n {f1.vsi.MakeVisitors()}");
            
            
        }
    }
}
