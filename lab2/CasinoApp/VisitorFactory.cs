using System;

namespace CasinoApp
{
    class VisitorFactory
    {
        private static VisitorFactory instance;

        private VisitorFactory() { }
        public static VisitorFactory getInstance()
        {
            if (instance == null)
                instance = new VisitorFactory();
            //Owner Owner = new Owner();
            return instance;
        }
        public string MakeVisitors()
        {
            Random r = new Random();
            int ind = r.Next(0, 10);
            if (ind >= 0 && ind <= 5)
            {
                StandartVisitor SV = new StandartVisitor();
                string rez = SV.Play();
                Console.WriteLine(rez);
                //Thread.Sleep(ind * 1000);
                return rez;
            }
            else
            {
                VIPVisitor VV = new VIPVisitor(new StandartVisitor());
                string rez = VV.Play();
                Console.WriteLine(rez);
                //Thread.Sleep(ind * 1000);
                return rez;
            }
            

        }
    }
}