namespace CasinoApp
{
    class GameForm
    {
        private static GameForm instance;

        private GameForm() { }

        public Owner Owner = Owner.getInstance();
        //public Area area = new Area();
        public Area area = Area.getInstance();
        public VisitorFactory vsi = VisitorFactory.getInstance();
        //public facade facade = new facade();
        
        public static GameForm getInstance()
        {
            if (instance == null)
                instance = new GameForm();
            //Owner Owner = new Owner();
            return instance;
        }

    }
}