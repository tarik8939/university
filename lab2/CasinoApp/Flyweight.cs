using System;
using System.Collections.Generic;
using System.Drawing;

namespace CasinoApp
{
    class Flyweight
    {
        public static Dictionary<Type, Image> Images = new Dictionary<Type, Image>();
        public static Image CrateInteriorItem1()
        {
            if (!Images.ContainsKey(typeof(InteriorItem1)))
            {
                Images.Add(typeof(InteriorItem1), Image.FromFile("image/int1.png"));
            }
            return Images[typeof(InteriorItem1)];
        }
        public static Image CrateInteriorItem2()
        {
            if (!Images.ContainsKey(typeof(InteriorItem2)))
            {
                Images.Add(typeof(InteriorItem2), Image.FromFile("image/int2.png"));
            }
            return Images[typeof(InteriorItem2)];
            
        }
        public static Image CrateInteriorItem3()
        {
            if (!Images.ContainsKey(typeof(InteriorItem3)))
            {
                Images.Add(typeof(InteriorItem3), Image.FromFile("image/int3.png"));
            }
            return Images[typeof(InteriorItem3)];
        }
        public static Image CratePlayingItem1()
        {
            if (!Images.ContainsKey(typeof(PlayingItem1)))
            {
                Images.Add(typeof(PlayingItem1), Image.FromFile("image/casino1.png"));
            }
            return Images[typeof(PlayingItem1)];
            
        }
        public static Image CratePlayingItem2()
        {
            if (!Images.ContainsKey(typeof(PlayingItem2)))
            {
                Images.Add(typeof(PlayingItem2), Image.FromFile("image/casino2.png"));
            }
            return Images[typeof(PlayingItem2)];
        }
        public static Image CratePlayingItem3()
        {
            if (!Images.ContainsKey(typeof(PlayingItem3)))
            {
                Images.Add(typeof(PlayingItem3), Image.FromFile("image/casino3.png"));
            }
            return Images[typeof(PlayingItem3)];
        }
    }
}