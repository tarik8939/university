﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoApp
{
    abstract class Mediator
    {
        public abstract void Send(string msg, Colleague colleague);
    }

    class ManagerMediator : Mediator
    {


        public Colleague Cook { get; set; }
        public Colleague officiant { get; set; }
        public override void Send(string msg, Colleague colleague)
        {

            if (officiant == colleague)
                Cook.Notify(msg);

            else if (Cook == colleague)
                officiant.Notify(msg);
            

        }
    }

    abstract class Colleague
    {
        protected Mediator mediator;

        public Colleague(Mediator mediator)
        {
            this.mediator = mediator;
        }

        public virtual void Send(string message)
        {
            mediator.Send(message, this);
        }
        public abstract void Notify(string message);
    }

    class Cook : Colleague
    {
        public Cook(Mediator mediator)
        : base(mediator)
        { }

        public override void Notify(string message)
        {
            Console.WriteLine("Message to cook: " + message);
        }
    }

    class officiant : Colleague
    {
        public officiant(Mediator mediator)
        : base(mediator)
        { }

        public override void Notify(string message)
        {
            Console.WriteLine("Message  to officiant: " + message);
        }
    }
}
