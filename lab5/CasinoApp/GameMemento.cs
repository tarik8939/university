using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CasinoApp
{
   
    class GameMemento
    {
    
        public List<itemtype> Items = new List<itemtype>();

        
        public  string TextBox { get; set; }

        public GameMemento(string text,List<itemtype> list)
        {
            this.TextBox = text;
            this.Items = list.ToList();
        }
    }
    
    [DataContract ]
    class GameHistory
    {
        [DataMember]
        public Stack<GameMemento> History { get; private set; }
        public GameHistory()
        {
            History = new Stack<GameMemento>();
        }
    }
    
}