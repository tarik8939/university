using System;
using System.Drawing;
using System.Linq;

namespace CasinoApp
{
public enum itemtype
    {
        interiorItem1,
        interiorItem2,
        interiorItem3,
        PlayingTable1,
        PlayingTable2,
        PlayingTable3


    }

    interface IItemFactory
    {
        
        IItem CreateItem1();
        IItem CreateItem2();
        IItem CreateItem3();
    }

    interface IItem
    {
        string created();
        string copied();
        itemtype GetItemtype();
        IItem Copy();
        int price { get;}
        int height { get; }
        int weight { get; }
    }
    
    class InteriorItem1 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } = 2;
        public int weight { get; } =2;
        Image Image = Flyweight.CrateInteriorItem1();
        public IItem Copy()
        {
            return (InteriorItem1)this.MemberwiseClone();
        }

        public itemtype GetItemtype()
        {
            return itemtype.interiorItem1;
        }

        public string created()
        {
          
            return "InteriorItem1 created";
        }
        public string copied()
        {

            return "InteriorItem1 copied";
        }
    }
    

    class InteriorItem2 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } =3;
        public int weight { get; } = 2;
        Image Image = Flyweight.CrateInteriorItem2();
        public IItem Copy()
        {
            return (InteriorItem2)this.MemberwiseClone();
        }
        public itemtype GetItemtype()
        {
            return itemtype.interiorItem2;
        }

        public string created()
        {
            return "InteriorItem2 created";
        }
        public string copied()
        {

            return "InteriorItem2 copied";
        }
    }
    class InteriorItem3 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } =2;
        public int weight { get; } =4;
        Image Image = Flyweight.CrateInteriorItem3();
        public IItem Copy()
        {
            return (InteriorItem3)this.MemberwiseClone();
        }
        public itemtype GetItemtype()
        {
            return itemtype.interiorItem3;
        }
        public string created()
        {
            return "InteriorItem3 created";
        }
        public string copied()
        {

            return "InteriorItem3 copied";
        }
    }

    class PlayingItem1 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } = 2;
        public int weight { get; } = 1;
        Image Image = Flyweight.CratePlayingItem1();
        public IItem Copy()
        {
            return (PlayingItem1)this.MemberwiseClone();
        }
        public itemtype GetItemtype()
        {
            return itemtype.PlayingTable1;
        }
 
        public string created()
        {
            
            //items.Add(PlayingItem1);
            return "PlayingItem1 created";
           
        }
        public string copied()
        {

            return "PlayingItem1 copied";
        }
    }

    class PlayingItem2 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } = 3;
        public int weight { get; } = 2;
        Image Image = Flyweight.CratePlayingItem2();
        public IItem Copy()
        {
            return (PlayingItem2)this.MemberwiseClone();
        }
        public itemtype GetItemtype()
        {
            return itemtype.PlayingTable2;
        }
        public string created()
        {
            return "PlayingItem2 created";
        }
        public string copied()
        {

            return "PlayingItem2 copied";
        }
    }
    class PlayingItem3 : IItem
    {
        public int price { get; } = 15000;
        public int height { get; } = 2;
        public int weight { get; } = 4;
        Image Image = Flyweight.CratePlayingItem3();
        public IItem Copy()
        {
            return (PlayingItem3)this.MemberwiseClone();
        }
        public itemtype GetItemtype()
        {
            return itemtype.PlayingTable3;
        }
        public string created()
        {
            return "PlayingItem3 created";
        }
        public string copied()
        {

            return "PlayingItem3 copied";
        }
    }



    class InteriorItemFactory : IItemFactory
    {
        Owner f1 = Owner.getInstance();

        public IItem CreateItem1()
        {
            IItem A;
            if (f1.AllItems.Contains(itemtype.interiorItem1) == true)
            {
                //A = (new Prototype()).Copy(f1.InteriorItem1.First());
                //A=f1.InteriorItem1.First().
                A = (f1.InteriorItem1.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new InteriorItem1();
                f1.InteriorItem1.Add(A);
                Console.WriteLine(A.created());
                
                
            }
            
            return A;
            //return new InteriorItem1();
        }
        public IItem CreateItem2()
        {
            //var A = new InteriorItem2();
            //f1.InteriorItem2.Add(A);

            IItem A;
            if (f1.AllItems.Contains(itemtype.interiorItem2) == true)
            {
                A = (f1.InteriorItem2.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new InteriorItem2();
                f1.InteriorItem2.Add(A);
                Console.WriteLine(A.created());
            }
            return A;
            //return new InteriorItem2();
        }
        public IItem CreateItem3()
        {
            //var A = new InteriorItem3();
            //f1.InteriorItem3.Add(A);
            IItem A;
            if (f1.AllItems.Contains(itemtype.interiorItem3) == true)
            {
                A = (f1.InteriorItem3.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new InteriorItem3();
                f1.InteriorItem3.Add(A);
                Console.WriteLine(A.created());
            }
            return A;
            //return new InteriorItem3();
        }
    }

    class  PlayingTableFactory : IItemFactory
    {

        Owner f1=Owner.getInstance();

        public IItem CreateItem1()
        {
            //var A = new PlayingItem1();
            //f1.PlayingTable1.Add(A);
            IItem A;
            if (f1.AllItems.Contains(itemtype.PlayingTable1) == true)
            {
                A = (f1.PlayingTable1.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new PlayingItem1();
                f1.PlayingTable1.Add(A);
                 Console.WriteLine(A.created());
            }
            return A;
            
            //return new PlayingItem1();
        }
        public IItem CreateItem2()
        {
            //var A = new PlayingItem2();
            //f1.PlayingTable2.Add(A);
            IItem A;
            if (f1.AllItems.Contains(itemtype.PlayingTable2) == true)
            {
                A = (f1.PlayingTable2.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new PlayingItem2();
                f1.PlayingTable2.Add(A);
                 Console.WriteLine(A.created());
            }
            return A;
            //return new PlayingItem2();
        }
        public IItem CreateItem3()
        {
            //var A = new PlayingItem3();
            //f1.PlayingTable3.Add(A);
            IItem A;
            if (f1.AllItems.Contains(itemtype.PlayingTable3) == true)
            {
                A = (f1.PlayingTable3.First()).Copy();
                Console.WriteLine(A.copied());
            }
            else
            {
                A = new PlayingItem3();
                f1.PlayingTable3.Add(A);
                 Console.WriteLine(A.created());
            }
            return A;
            //return new PlayingItem3();
        }
    }
}