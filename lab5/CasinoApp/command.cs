﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoApp
{
    public interface ICommand
    {
        void Execute();
    }


    class Command : ICommand
    {
        private string _payload = string.Empty;

        public Command(string payload)
        {
            this._payload = payload;
        }

        public void Execute()
        {
            Console.WriteLine($" Officiant: Carrying the order");
            //Console.WriteLine($"SimpleCommand: See, I can do simple things like printing ({this._payload})");
        }
    }


    class ComplexCommand : ICommand
    {
        private Receiver _receiver;


        private string _a;

        private string _b;


        public ComplexCommand(Receiver receiver, string a)
        {
            this._receiver = receiver;
            this._a = a;
     
        }

        
        public void Execute()
        {
            
            this._receiver.DoSomething(this._a);
            this._receiver.DoSomethingElse(this._a);
        }
    }

    class Receiver
    {
        public void DoSomething(string a)
        {
            Console.WriteLine($"Receiver: Working on {a}");
        }

        public void DoSomethingElse(string a)
        {
            Console.WriteLine($"Receiver: {a} is ready");
        }
    }

    
    class Invoker
    {
        public ManagerMediator mediator = new ManagerMediator();
        Colleague officiant = new officiant(new ManagerMediator());
        Colleague Cook = new Cook(new ManagerMediator());
        //Owner f1 = Owner.getInstance();
        
        private ICommand _onStart;

        private ICommand _onFinish;

        // Initialize commands.
        public void SetOnStart(ICommand command)
        {

            this._onStart = command;
            
        }

        public void SetOnFinish(ICommand command)
        {
            this._onFinish = command;
        }


        public void DoSomethingImportant()
        {
           
            Console.WriteLine("Visitor: Make order");
            mediator.officiant = officiant;
            mediator.Cook = Cook;
            
            if (this._onStart is ICommand)
            {
                this._onStart.Execute();
                mediator.Send("prepare an order", officiant);

            }

            if (this._onFinish is ICommand)
            {
                this._onFinish.Execute();
                mediator.Send("pick up the order", Cook);
            }
        }
    }


}
