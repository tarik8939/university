namespace CasinoApp
{
    class GameFormThreading
    {
        private GameFormThreading() { }

        private static GameFormThreading instance;

        public Owner Owner = Owner.getInstance();

        private static readonly object _lock = new object();

        public static GameFormThreading GetInstance(string value)
        {

            if (instance == null)
            {
            
                lock (_lock)
                {

                    if (instance == null)
                    {
                        instance = new GameFormThreading();
                        instance.Value = value;
                    }
                }
            }
            return instance;
        }
        public string Value { get; set; }

    }
}