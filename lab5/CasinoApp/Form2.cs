﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Text.Json;
using System.Threading.Tasks;

namespace CasinoApp
{
    public partial class Form2 : Form
    {
        GameForm f1;
        GameForm f2;

        GameFormThreading ft1;
        GameFormThreading ft2;
        GameHistory game=new GameHistory();
        public Form2()
        {
            InitializeComponent();
            f1 = GameForm.getInstance();
            f2 = GameForm.getInstance();
            //MessageBox.Show(a);


        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //Invoker invoker = new Invoker();
            //invoker.SetOnStart(new Command("Order"));
            //Receiver receiver = new Receiver();
            //invoker.SetOnFinish(new ComplexCommand(receiver, "Order"));
            //invoker.DoSomethingImportant();

            if (f1 == f2)
            {
                Console.WriteLine("Singleton works.");
                
            }
            else
            {
                Console.WriteLine("Singleton failed.");
            }



            Thread process1 = new Thread(() =>
            {
                TestSingleton("FOO");
            });
            Thread process2 = new Thread(() =>
            {
                TestSingleton("BAR");
            });

            process1.Start();
            process2.Start();

            process1.Join();
            process2.Join();
            
            facade facade = new facade();
            facade.SetAll();
            
        }

        public static void TestSingleton(string value)
        {
            GameFormThreading singleton = GameFormThreading.GetInstance(value);
            Console.WriteLine(singleton.Value);
        }




        private void button3_Click(object sender, EventArgs e)
        {

            string item = Convert.ToString(comboBox1.SelectedItem);

            if (item == "item1")
            {
                //Console.WriteLine("asdafaf");
                richTextBox1.AppendText(f1.Owner.BuyItem1(new InteriorItemFactory()));
                
            }
            else if (item == "item2")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem2(new InteriorItemFactory()));
            }
            else if (item == "item3")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem3(new InteriorItemFactory()));
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string item2 = Convert.ToString(comboBox2.SelectedItem);
            if (item2 == "item1")
            {
                richTextBox1.AppendText( f1.Owner.BuyItem1(new PlayingTableFactory()));
            }
            else if (item2 == "item2")
            {
                richTextBox1.AppendText(f1.Owner.BuyItem2(new PlayingTableFactory()));
            }
            else if (item2 == "item3")
            {
               richTextBox1.AppendText( f1.Owner.BuyItem3(new PlayingTableFactory()));

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //richTextBox1.AppendText($"\n {f1.vsi.MakeVisitors()}");
            f1.vsi.MakeVisitors();




        }



        private void  button4_Click(object sender, EventArgs e)
        {

            f1.Owner.textbox = richTextBox1.Text;
            game.History.Push(f1.Owner.SaveState());

//            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(GameMemento));
//            MemoryStream stream = new MemoryStream();
//            XmlWriter writer = new XmlTextWriter("test.xml", null);
//            serializer.WriteObject(stream, obj);
//            serializer.WriteObject(writer, obj);
//            writer.Close();


//            using (FileStream fs = new FileStream("user.json", FileMode.OpenOrCreate))
//            {
//                  JsonSerializer.SerializeAsync<GameHistory>(fs, game);
//                //Console.WriteLine("Data has been saved to file");
//            }

//            using (FileStream fs = File.Create("user.json"))
//            {
//                 JsonSerializer.SerializeAsync(fs, game);
//            }

        }

        private  void button5_Click(object sender, EventArgs e)
        {
            f1.Owner.RestoreState(game.History.Pop());
            //MessageBox.Show(Convert.ToString(f1.Owner.AllItems.Count()));
            richTextBox1.Text = f1.Owner.textbox;
            
        }

        private async void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            using (FileStream fs = new FileStream("user.json", FileMode.OpenOrCreate))
            {
                await JsonSerializer.SerializeAsync<GameHistory>(fs, game);
                //Console.WriteLine("Data has been saved to file");
            }
        }
    }
}
