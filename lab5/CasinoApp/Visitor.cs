using System;

namespace CasinoApp
{
abstract class Visitor
    {
        public abstract void Play();

    }
    class StandartVisitor : Visitor
    {
        public string type = "Standart visitor";
        public override void Play()
        {
            Console.WriteLine( $"{type}:wow i can playing a game");
        }
    }
    abstract class Decorator : Visitor
    {
        protected Visitor _visitor;
        public Decorator(Visitor visitor)
        {
            this._visitor = visitor;
        }
        public void VisitorDecorator(Visitor visitor)
        {
            this._visitor = visitor;
        }
        public override void Play()
        {
            if (this._visitor != null)
            {
                 this._visitor.Play();
            }
            else
            {
                //string.Empty;
            }
        }
    }
    class VIPVisitor : Decorator
    {
        public VIPVisitor(Visitor vst) : base(vst)
        {
        }

        private void MakeOrder()
        {

            var order= new Order(new Making());
             order.Request1();
            order.Request2();

            //return a;
        }
        
        public string type = "VIP visitor";
        private void GoToBar()
        {
            Console.WriteLine( "I'm gonna go to bar");
        }
        private void GoToRestaurant()
        {
            
            Console.WriteLine( $"I'm gonna go to restaurant. My order \n");

            Invoker invoker = new Invoker();
            invoker.SetOnStart(new Command("Order"));
            Receiver receiver = new Receiver();
            
            invoker.SetOnFinish(new ComplexCommand(receiver, "Order")); 
            invoker.DoSomethingImportant();
            MakeOrder();



        }
        private void GoBack()
        {
            Console.WriteLine( "I don't want to go in bar and restaurant and bar");
        }
        public void Where()
        {
            Random r = new Random();
            int ind = r.Next(-1, 3);
            //Console.WriteLine(ind);
            if (ind == 0)
            {
                 GoBack();
            }
            else if (ind == 1)
            {
                 GoToBar();
            }
            else  GoToRestaurant();
        }
        //public override string Play()
        //{
        //    //string rez = $"wow i can playing a game \n {Where()}";
        //    return $"{type}:wow i can playing a game, hm.... {Where()}";
        //}
        public override void Play()
        {
            //string a= _visitor.Play(); 
            Console.Write( $"{type}:");
            _visitor.Play();
            Console.Write(", hmmm.....");
            Where();
        }
    }
}