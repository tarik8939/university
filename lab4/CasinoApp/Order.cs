using System;

namespace CasinoApp
{
    
    
    abstract class State
    {
        protected Order _context;

        public void SetContext(Order context)
        {
            this._context = context;
        }

        public abstract void Handle1();

        public abstract void Handle2();
        
    }
    
    
    
    class Order
    {
        private State _state = null;
        
        public Order(State state)
        {
            this.TransitionTo(state);
        }
        
        public void TransitionTo(State state)
        {
            Console.WriteLine($"Order: Transition to {state.GetType().Name}.");
            this._state = state;
            this._state.SetContext(this);
        }
        public void Request1()
        {
            this._state.Handle1();

        }

        public void Request2()
        {
            this._state.Handle2();

        }
        
    }
    
    class Making : State
    {
        public override void Handle1()
        {
            Console.WriteLine("State - making");

            this._context.TransitionTo(new Ready());

        }

        public override void Handle2()
        {
            //Console.WriteLine("ConcreteStateA handles request2.");

        }
    }
    
    class Ready : State
    {
        public override void Handle1()
        {
            //Console.Write("ConcreteStateB handles request1.");
        }

        public override void Handle2()
        {
            Console.WriteLine("State - ready.");
            //this._context.TransitionTo(new Making());
            
        }
    }
}