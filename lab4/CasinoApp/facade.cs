

using System;

namespace CasinoApp
{
    class facade
    {
        GameForm f1;
        public facade()
        {
            f1 = GameForm.getInstance();
        }
        Restaurant createRest()
        {
            Console.WriteLine("Restourant created");
            return new Restaurant();
        }
        Kitchen createKitchen()
        {
            Console.WriteLine("Kitchen created");
            return new Kitchen();
        }
        Bar createBar()
        {
            Console.WriteLine("Bar created");
            return new Bar();
        }
        public void SetAll()
        {
            f1.area.rest = createRest();
            f1.area.bar = createBar();
            f1.area.kitchen = createKitchen();
 
        }
    }
}