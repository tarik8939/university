using System;

namespace CasinoApp
{
    class VisitorFactory
    {
        private static VisitorFactory instance;

        private VisitorFactory() { }
        public static VisitorFactory getInstance()
        {
            if (instance == null)
                instance = new VisitorFactory();
            //Owner Owner = new Owner();
            return instance;
        }
        public void MakeVisitors()
        {
            Random r = new Random();
            int ind = r.Next(0, 10);
            if (ind >= 0 && ind <= 5)
            {
                StandartVisitor SV = new StandartVisitor();
                SV.Play();
                
            }
            else
            {
                VIPVisitor VV = new VIPVisitor(new StandartVisitor());
                VV.Play();
            }
            

        }

    }
}