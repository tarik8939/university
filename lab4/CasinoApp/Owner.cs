using System;
using System.Collections.Generic;
using System.Linq;

namespace CasinoApp
{
    class Owner : IPeople
    {




        private static Owner instance;
        private Owner() { }

        public static Owner getInstance()
        {
            if (instance == null)
                instance = new Owner();
            return instance;
        }

        public string name { get; }
        public string surname { get; }
        public int Age { get; }
        private int Budget { get; } = 99999999;
        public List<itemtype> AllItems = new List<itemtype>();
        public List<IItem> InteriorItem1 = new List<IItem>();
        public List<IItem> InteriorItem2 = new List<IItem>();
        public List<IItem> InteriorItem3 = new List<IItem>();
        public List<IItem> PlayingTable1 = new List<IItem>();
        public List<IItem> PlayingTable2 = new List<IItem>();
        public List<IItem> PlayingTable3 = new List<IItem>();
        public string textbox;



        public string BuyItem1(IItemFactory factory)
        {   
            var A = factory.CreateItem1();
            AllItems.Add((A.GetItemtype()));
            return $"{Convert.ToString(A.GetItemtype())}\n";
        }
        public string BuyItem2(IItemFactory factory)
        {
            var A = factory.CreateItem2();
            AllItems.Add((A.GetItemtype()));
            return $"{Convert.ToString(A.GetItemtype())}\n";
        }
        public string BuyItem3(IItemFactory factory)
        {
            var A = factory.CreateItem3();
            AllItems.Add((A.GetItemtype()));
            return $"{Convert.ToString(A.GetItemtype())}\n";

        }
        
        public GameMemento SaveState()
        {
            
            return new GameMemento(textbox, AllItems);
        }
        
        public void RestoreState(GameMemento memento)
        {
            this.AllItems=memento.Items.ToList();
            this.textbox = memento.TextBox;
            //this.lives = memento.Lives;
            

        }





    }
}