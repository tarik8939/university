namespace CasinoApp
{
    class Area
    {
        private static Area instance;

        private Area() { }
        public static Area getInstance()
        {
            if (instance == null)
                instance = new Area();
            //Owner Owner = new Owner();
            return instance;
        }

        public Restaurant rest;
        public Kitchen kitchen;
        public Bar bar;
    }
}