using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
//path:   C:\tmp\1.txt
namespace InterTest
{
    public class StandartLexer
    {

        public enum Token
        {
            System ,

            NewFile ,
            DelateFile ,
            CopyFile ,
            CutFile ,
            WriteIn ,
            Find ,
            Text,
            Path ,
            Path2 ,

        }

        private List<string> TokenList = new List<string>();


        private void tree()
        {
            Console.WriteLine("tree:");
            for (int i = 0; i < TokenList.Count; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    Console.Write("\t");
                }
                Console.WriteLine(TokenList[i]);
            }
        }
        private string createPath(string line)
        {
            string path = "";
            if (line[0] == 'C')
            {
                path = @"C:";
                line = line.Replace("C:", "");
                path += line;
            }
            else if (line[0] == 'D')
            {
                path = @"D:";
                line = line.Replace("D:", "");
                path += line;
            }
            else throw new Exception("You write incorrect path");

            return path;
        }

        public void WriteToken()
        {
         Console.Write("Your token: ");
         for (int i = 0; i <TokenList.Count; i++)
         {
             Console.Write(TokenList[i]+" ");
         }
        }
        private string WritePath()
        {
            Console.WriteLine("Write path");
            string path = Console.ReadLine();
            return path;
        }

        private void Find()
        {

            string path = WritePath();
            Console.WriteLine("Write file type");
            string file_type = "*."+Console.ReadLine();
            TokenList.Add(Convert.ToString(Token.Path) +":"+path);
            
            string[] fileNames = Directory.GetFiles(path, file_type, SearchOption.AllDirectories);
            Console.WriteLine("Amount of files: "+fileNames.Length);
            
        }
        
        private void Write()
        {
            Console.WriteLine("Write text");
            string text = Console.ReadLine();
            string path = WritePath();
            try
            {
                if (path == "")
                {
                    throw new Exception("Write correct path");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            path = createPath(path);
            TokenList.Add(Convert.ToString(Token.Path)+":"+path);
            TokenList.Add(Convert.ToString(Token.Text)+":"+text);
            Console.WriteLine("Operation success");
            File.WriteAllText(path, text);
        }

        private void NewFile()
        {

            string line = WritePath();
            try
            {
                if (line.Length==0)
                {
                    throw new Exception("Write correct path");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            string path = createPath(line);
            TokenList.Add(Convert.ToString(Token.Path)+":"+path);
            Console.WriteLine("File created");
            File.Create(path);
            tree();
        }

        private void Delete()
        {
            string line = WritePath();
            try
            {
                if (line == "")
                {
                    throw new Exception("Write correct path");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
            string path = createPath(line);
            TokenList.Add(Convert.ToString(Token.Path)+":"+path);
            Console.WriteLine("File delated");
            File.Delete(path);
            tree();

        }

        private void Copy()
        {
            string from = WritePath();
            string to = WritePath();
            try
            {
                if (from == "" || to == "")
                {
                    throw new Exception("Write correct path");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            string pathFrom = createPath(from);
            string pathTo = createPath(to);
            pathTo += from.Remove(0, from.LastIndexOf(@"\"));
            TokenList.Add(Convert.ToString(Token.Path)+":"+pathFrom);
            TokenList.Add(Convert.ToString(Token.Path2)+":"+pathTo);
            Console.WriteLine("File copied");
            File.Copy(pathFrom, pathTo, true);
        }

        private void Cut()
        {
            string from = WritePath();
            string to = WritePath();
            try
            {
                if (from == "" || to == "")
                {
                    throw new Exception("Write correct path");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
            string pathFrom = createPath(from);
            string pathTo = createPath(to);
            pathTo += from.Remove(0, from.LastIndexOf(@"\"));
            TokenList.Add(Convert.ToString(Token.Path)+":"+pathFrom);
            TokenList.Add(Convert.ToString(Token.Path2)+":"+pathTo);
            Console.WriteLine("File cuted");
            File.Move(pathFrom, pathTo);
            tree();
        }

        public void Lexer(string[] arr)
        {
            TokenList.Clear();

            int i = 0;
            switch (arr[i])
            {
                case "System":
                    TokenList.Add((Convert.ToString(Token.System)));
                    i++;
                    switch (arr[i])
                    {
                        case "NewFile":
                            TokenList.Add(Convert.ToString(Token.NewFile));
                            NewFile();
                        
                            break;
                        case "DelateFile":
                            TokenList.Add(Convert.ToString(Token.DelateFile));

                                Delete();


                            break;
                        case "CopyFile":
                            TokenList.Add(Convert.ToString(Token.CopyFile));

                                Copy();


                            break;
                        case "CutFile":
                            TokenList.Add(Convert.ToString(Token.CutFile));

                                Cut();


                            break;
                        case "WriteIn":
                            TokenList.Add(Convert.ToString(Token.WriteIn));

                                Write();

                            break;
                        case "Find" :
                            TokenList.Add(Convert.ToString(Token.Find));
                            Find();
                            break;
                            

                        default:
                            throw new Exception("write correct command");
                    }
                    break;
                default:
                    throw new Exception("write correct command");
            }
        }
    }
}